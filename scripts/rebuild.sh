#!/bin/bash
# Copyright 2020 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -e

script_path="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
headers_path=$script_path/..

if [[ $# -lt 2 ]]
then
  echo "Usage: rebuild.sh <Vulkan-Docs path> <Vulkan-Hpp path>"
  exit 1
fi
vulkan_docs_path=$1
vullkan_hpp_path=$2

cd $vulkan_docs_path/xml
cp -v $headers_path/registry/vk.xml .
make
cp -v ../gen/include/vulkan/vulkan_core.h $headers_path/include/vulkan
cp -v ../gen/include/vulkan/vulkan_fuchsia.h $headers_path/include/vulkan

cd $vullkan_hpp_path
./VulkanHppGenerator $headers_path/registry/vk.xml

case "$OSTYPE" in
  darwin*)  arch="mac-x64" ;;
  linux*)   arch="linux-x64" ;;
  *)        echo "Unknown: $OSTYPE"; exit ;;
esac

echo Formatting...
$headers_path/../../prebuilt/third_party/clang/${arch}/bin/clang-format --style=file:.clang-format_11 -i vulkan/vulkan*.hpp
cp -v vulkan/vulkan*.hpp $headers_path/include/vulkan
